# Pouyan

Welcome to my personal GitLab profile! :wave:
I am a data analyst and a machine learning researcher with M.Sc. degree of software engineering. I'm interested in data science and artificial intelligence.

<p align="center">
    <a href="https://p74.ir">Website</a>
    ·
    <a href="https://gitlab.com/p74/p74/-/blob/master/README_Repo.md">Website README</a>
</p>

## Primary Skills

- Machine learning
- Data Analysis
- Python
- SQL (OracleDB)
- Tableau


## Contact

You can reach me at my personal website: [p74.ir/contact](https://p74.ir/contact)

## Contributions

Pull requests are welcome. If you have any suggestions or feedback on how to improve the website, feel free to open an issue or a pull request.

## License

Unless otherwise stated, all of my works under this account are licensed under the X11 license. See the [LICENSE](https://gitlab.com/p74/p74/-/blob/master/LICENSE) file for more details.